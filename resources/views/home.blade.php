@extends('layouts.app')

@section('content')
<div class="container">


    <passport-clients></passport-clients>
    <br /><br /><br />
    <passport-authorized-clients></passport-authorized-clients>
    <br /><br /><br />
    <passport-personal-access-tokens></passport-personal-access-tokens>
                
</div>
@endsection
