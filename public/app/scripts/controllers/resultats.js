'use strict';

/**
 * @ngdoc function
 * @name autostopApp.controller:ResultatsController
 * @description
 * # ResultatsController
 * Controller of the autostopApp
 */
angular.module('autostopApp')
.controller('ResultatsController', [
    '$scope',
    'MapboxAPIService',
    function ( $scope, MapboxAPIService ) {

        var mapboxgl = window.mapboxgl;

        var map = new mapboxgl.Map({
            container: 'amap', // container id
            style: 'mapbox://styles/mapbox/streets-v9', //hosted style id
            center: [4.491286, 44.884790], // starting position
            zoom: 10 // starting zoom
        });

        

        
        console.log( map );

        $scope.journey = {
            nbPlaces: 3,
            start: {
                location: {
                    name: 'Le Cheylard, 07160',
                    coordinates: [4.491286, 44.884790],
                    city: 'Le Cheylard'
                }
            },
            end: {
                location: {
                    name: 'Privas, 07000',
                    coordinates: [ 4.599039,44.735269],
                    city: 'Privas'
                }
            }
        };
        
        // Aller chercher l'itinéraire et l'afficher
        MapboxAPIService.getDirection( [
            $scope.journey.start.location.coordinates,
            $scope.journey.end.location.coordinates,
        ] ).then(function( data ) {
            map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': {
                    'type': 'geojson',
                    'data': {
                        'type': 'Feature',
                        'properties': {},
                        'geometry': {
                            'type': 'LineString',
                            'coordinates': data.routes[0].geometry.coordinates
                        }
                    }
                },
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#888',
                    'line-width': 8
                }
            });
        });

        $scope.items = [
            {
                '_id': '5a782dc0e5882208022c6808',
                'user': {
                    'id': '5a782dc07d3bf1a696d7f376',
                    'name': 'Manon Gauthier'
                },
                'journey': {
                  'start': {
                    'location': {
                        'name': 'Le Village, 07160 Saint-Jean-Roure',
                        'coordinates': [
                            4.428030,
                            44.940539
                        ]
                    },
                    'date': 'February 08, 2018 09:15:00',
                },
                'end': {
                    'location': {
                        'name': 'Privas 07000',
                        'coordinates': [
                            4.599039,
                            44.735269
                        ]
                    },
                    'date': 'February 08, 2018 10:35:00',
                    'time': {
                      'hour': 14,
                      'minute': 21
                    }
                  }
                }
            },
            {
                '_id': '5a782dc0e5882208022c6808',
                'user': {
                    'id': '5a782dc07d3bf1a696d7f376',
                    'name': 'Timéo Brun'
                },
                'journey': {
                  'start': {
                    'location': {
                        'name': 'La Chèze, 07160 Le Cheylard',
                        'coordinates': [
                            4.430715,
                            44.906158
                        ]
                    },
                    'date': 'February 08, 2018 8:30:00',
                  },
                  'end': {
                    'location': {
                        'name': 'Privas 07000',
                        'coordinates': [
                            4.599039,
                            44.735269
                        ]
                    },
                    'date': 'February 08, 2018 10:00:00',
                    'time': {
                      'hour': 14,
                      'minute': 21
                    }
                  }
                }
            },
            {
                '_id': '5a782dc0e5882208022c6808',
                'user': {
                    'id': '5a782dc07d3bf1a696d7f376',
                    'name': 'Jean-Bernard Huet'
                },
                'journey': {
                  'start': {
                    'location': {
                        'name': 'La Brugière, 07190 Saint-Sauveur-de-Montagut',
                        'coordinates': [
                            4.582509,
                            44.820274
                        ]
                    },
                    'date': 'February 08, 2018 09:15:00',
                  },
                  'end': {
                    'location': {
                        'name': 'Privas 07000',
                        'coordinates': [
                            4.599039,
                            44.735269
                        ]
                    },
                    'date': 'February 08, 2018 10:15:00',
                    'time': {
                      'hour': 14,
                      'minute': 21
                    }
                  }
                }
            },
        ];
        var el, marker, popup;
        $scope.items.forEach( function( item ) {
            item.journey.start.date = new Date( item.journey.start.date );
            item.journey.end.date = new Date( item.journey.end.date );

            // on créer le markeur de l'utilisateur
            el = document.createElement('i');
            el.className = 'fa fa-user fa-2x marker';
            el.style.width = '25px';
            el.style.height = '25px';

            popup = new mapboxgl.Popup()
                .setHTML('<div><p class="popup-name strong">' + item.user.name + '</p><p class="popup-address">' + item.journey.start.location.name + '</p></div>');

            marker = new mapboxgl.Marker(el);
            marker.setLngLat( item.journey.start.location.coordinates );
            marker.setPopup( popup );
            marker.addTo( map );

        });

        // on créer le markeur de départ
        el = document.createElement('i');
        el.className = 'fa fa-neuter fa-2x marker';
        el.style.width = '25px';
        el.style.height = '25px';

        popup = new mapboxgl.Popup()
            .setHTML('<div><p class="popup-name strong">Vous</p><p class="popup-address">Le Cheylard, 07160</p></div>');

        marker = new mapboxgl.Marker(el);
        marker.setLngLat( $scope.journey.start.location.coordinates );
        marker.setPopup( popup );
        marker.addTo( map );

        // on créer le markeur de l'arrivée
        el = document.createElement('i');
        el.className = 'fa fa-flag-checkered fa-2x marker';
        el.style.width = '25px';
        el.style.height = '25px';

        popup = new mapboxgl.Popup()
            .setHTML('<div><p class="popup-name strong">Arrivée</p><p class="popup-address">Privas, 07000</p></div>')       ;

        marker = new mapboxgl.Marker(el);
        marker.setLngLat( $scope.journey.end.location.coordinates );
        marker.setPopup( popup );
        marker.addTo( map );

        $scope.showInTheMap = function( index ) {
            map.flyTo({
                center: $scope.items[ index ].journey.start.location.coordinates,
                zoom: 11
            });
        };
        
        $scope.showStart = function() {
            map.flyTo({
                center: $scope.journey.start.location.coordinates,
                zoom: 11
            });
        };
        $scope.showEnd = function() {
            map.flyTo({
                center: $scope.journey.end.location.coordinates,
                zoom: 11
            });
        };
        
        $scope.formatDate = function( value ) {
            if( value < 10 ) {
                value = '0' + value;
            }

            return value;
        };
    }
]);
