'use strict';

/**
 * @ngdoc function
 * @name autostopApp.controller:PostJourneyController
 * @description
 * # PostJourneyController
 * Controller of the autostopApp
 */
angular.module('autostopApp')
.controller('PostJourneyController', [
    '$scope',
    '$q',
    'MapboxAPIService',
    'AutostopAPIService',
    '$location',
    '$cookies',
    function ( $scope, $q, MapboxAPIService, AutostopAPIService, $location, $cookies ) {
        
        /**
         * Initialisation
         * Parametrage du controlleur en fonction de l'url
         */
        function init() {
            // Vérification de l'authentification
            if( ! $cookies.get( 'userId' ) || ! $cookies.get( 'userName' ) ) {
                $location.url( '/?redirect=not-register' );
            }

            // Initialisation des step éventuel
            if( $location.url() === '/rechercher-trajet' ) {
                $scope.IsASearch = true;
            } else if( $location.url() === '/proposer-trajet' ) {
                $scope.IsASearch = false;
                $scope.steps = [
                    {
                        'doSearch': false,
                        'locations': MapboxAPIService.getEmptyLocations(),
                        'locationsOld': MapboxAPIService.getEmptyLocations(),
                        'address': '',
                        'last': true
                    }
                ];
            } else {
                console.error( 'url inconnus !' );
            }
            
        }
        init();


        // pour le datePopup
        $scope.datePopup = { 
            'opened': false 
        };

        
        $scope.addressStart = {
            'doSearch': false,
            'locations': MapboxAPIService.getEmptyLocations(),
            'locationsOld': MapboxAPIService.getEmptyLocations(),
            'address': ''
        };
        $scope.addressEnd = {
            'doSearch': false,
            'locations': MapboxAPIService.getEmptyLocations(),
            'locationsOld': MapboxAPIService.getEmptyLocations(),
            'address': ''
        };
        
        // Données de l'annonce (info du user, te données du formulaire)
        $scope.journey = {
            'user': {
                'id': 1,
                'userName': 'toto'
            },
            'time': {
                'heure': '',
                'minute': '',
                'date': new Date()
            },
            'addressStart': $scope.addressStart.address,
            'addressEnd': $scope.addressEnd.address
        };

    
    
        /**
         * Permet d'ouvrir le DatePopup
         */
        $scope.openDatePopup = function() {
            $scope.datePopup.opened = true;
        };

        /**
         * Permet de lancer (si le dosearch est positif) une requete à l'api mapbox 
         * et de mettre à jour les résultats de l'autocompletions
         */
        var changeAddress = function ( query ) {
            var deferred = $q.defer();

            MapboxAPIService.search( query ).then(function (locations) {
                deferred.resolve( locations );
            });

            return deferred.promise;
        };
        $scope.onChangeAddressStart = function () {
            if( $scope.addressStart.doSearch > 0 ) {
                changeAddress( $scope.addressStart.address )
                .then( function( locations ) {
                    $scope.addressStart.locations = JSON.parse( JSON.stringify( locations ) );
                });
            }

            return;
        };
        $scope.onChangeAddressEnd = function () {
            if( $scope.addressEnd.doSearch > 0 ) {
                changeAddress( $scope.addressEnd.address )
                .then( function( locations ) {
                    $scope.addressEnd.locations = JSON.parse( JSON.stringify( locations ) );
                });
            }

            return;
        };
        $scope.onChangeAddressStep = function ( index ) {
            if( $scope.steps[ index ].doSearch > 0 ) {
                changeAddress( $scope.steps[ index ].address )
                .then( function( locations ) {
                    $scope.steps[ index ].locations = JSON.parse( JSON.stringify( locations ) );
                });
            }
        };
        
        /**
         * Permet de vérifier que la touche pressé n'est pas une fleche ou entrée 
         * (pour faire bouger le curseur ou valider)
         */
        var keypressAddress = function( obj, keysEvent ) {
            var KC_ARROW_UP    = 38;
            var KC_ARROW_DOWN  = 40;
            var KC_ENTER       = 13;
            
            switch ( keysEvent.keyCode ) {
                case KC_ARROW_UP:
                    obj.locations.up();
                    obj.doSearch = -1;
                break;
                    
                case KC_ARROW_DOWN:
                    obj.locations.down();
                    obj.doSearch = -1;
                break;
                    
                case KC_ENTER:
                    obj.address = obj.locations.places[ obj.locations.selected ].name;
                    obj.doSearch = 1;

                    // On clone l'objet
                    obj.locationsOld = JSON.parse( JSON.stringify( obj.locations ) );
                    obj.locations.places = [];
                break;
                    
                default:
                    obj.doSearch = 1;
                break;
            }

            return obj;
        };
        $scope.onKeypressAddressStart = function( keysEvent ) {
            $scope.addressStart = JSON.parse( JSON.stringify( keypressAddress( $scope.addressStart, keysEvent ) ) );
        };
        $scope.onKeypressAddressEnd = function( keysEvent ) {
            $scope.addressEnd = JSON.parse( JSON.stringify( keypressAddress( $scope.addressEnd, keysEvent ) ) );
        };
        $scope.onKeypressAddressStep = function( index, keysEvent ) {
            $scope.steps[ index ] = JSON.parse( JSON.stringify( keypressAddress( $scope.steps[ index ], keysEvent ) ) );
        };
        


        /**
         * Permet de vérifier si l'on a pas cliqué sur une sugggestion de l'autocomplete
         * Si c'est le cas on sélectionne la suggestion et mets à jour le tableau location
         */
        var onBlurAddress = function( obj, clickEvent ) {
            // on commence par referme la liste des suggestions
            if( obj.locations.places.length !== 0 ) {
                obj.locationsOld = JSON.parse( JSON.stringify( obj.locations ) );
                obj.locations.places = [];
            }
            
            var deferred = $q.defer();

            // On vérifie si en sortant, on a pas cliquer sur une suggestion
            if( clickEvent.relatedTarget !== null ) {
                var target = angular.element( clickEvent.relatedTarget );
                
                if( target.attr('class').search( 'suggestion-autocomplete' ) !== -1 ) {
                    obj.address = target.children('.placeName').text();
                    MapboxAPIService.search( obj.address ).then(function (locations) {
                        obj.locationsOld = JSON.parse( JSON.stringify( locations ) );
                        deferred.resolve(obj);
                    });
                } else {
                    deferred.resolve( obj );
                } 
            } else {
                deferred.resolve( obj );
            }
            
            return deferred.promise;  
        };
        $scope.onBlurAddressStart = function( clickEvent ) {
            onBlurAddress( $scope.addressStart, clickEvent ).then( function( obj ) {
                $scope.addressStart = JSON.parse( JSON.stringify( obj ) );
            });
        };
        $scope.onBlurAddressEnd = function( clickEvent ) {
            onBlurAddress( $scope.addressEnd, clickEvent ).then( function( obj ) {
                $scope.addressEnd = JSON.parse( JSON.stringify( obj ) );
            });
        };
        $scope.onBlurAddressStep = function( index, clickEvent ) {
            onBlurAddress( $scope.steps[ index ], clickEvent ).then( function( obj ) {
                $scope.steps[ index ] = JSON.parse( JSON.stringify( obj ) );
            });
        };

        /**
         * Permet de réafficher la liste d'autocompletion 
         */
        $scope.onFocusAddressStart = function() {
            if( $scope.addressStart.locationsOld.length !== 0) {
                $scope.addressStart.locations = JSON.parse( JSON.stringify( $scope.addressStart.locationsOld ) );
            }
        };
        $scope.onFocusAddressEnd = function() {
            if( $scope.addressEnd.locationsOld.length !== 0) {
                $scope.addressEnd.locations = JSON.parse( JSON.stringify( $scope.addressEnd.locationsOld ) );
            }
        };
        $scope.onFocusAddressStep = function( index ) {
            if( $scope.steps[ index ].locationsOld.length !== 0) {
                $scope.steps[ index ].locations = JSON.parse( JSON.stringify( $scope.steps[ index ].locationsOld ) );
            }
        };

        /**
         * Permet de gérer les étapes (Uniquement pour la proposition de trajet)
         */
        $scope.newStep = function() {
            $scope.steps[ $scope.steps.length - 1 ].last = false;

            $scope.steps.push({
                'doSearch': false,
                'locations': MapboxAPIService.getEmptyLocations(),
                'locationsOld': MapboxAPIService.getEmptyLocations(),
                'address': '',
                'last': true
            });
        };
        $scope.removeStep = function( index ) {
            $scope.steps.splice( index, 1 );
        };

        /**
         * Méthode de validation
         */
        $scope.publish = function () {

            var type = 'driver';
            var date = $scope.journey.time.date.getFullYear() +
                '-' + ( $scope.journey.time.date.getMonth() + 1) +
                '-' + $scope.journey.time.date.getDate() +
                ' ' + $scope.journey.time.hours +
                ':' + $scope.journey.time.minutes +
                ':00';

            if( $scope.IsASearch ) {
                type = 'passenger';
            }

            AutostopAPIService.newJourney({
                'user_id': 1,
                'type': type,
                'address_start': $scope.addressStart,
                'address_end': $scope.addressEnd,
                'date_start': date
            });
           

        };
    }
]);
