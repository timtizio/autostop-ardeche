'use strict';

/**
 * @ngdoc overview
 * @name autostopApp
 * @description
 * # autostopApp
 *
 * Main module of the application.
 */
angular
  .module('autostopApp', [
    'ngRoute',
    'ngAnimate',
    'ngCookies',
    'ui.bootstrap',
    'ngSanitize'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/views/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/rechercher-trajet', {
        templateUrl: 'app/views/postJourney.html',
        controller: 'PostJourneyController',
        controllerAs: 'about'
      })
      .when('/proposer-trajet', {
        templateUrl: 'app/views/postJourney.html',
        controller: 'PostJourneyController',
        controllerAs: 'about'
      })
      .when('/inscription', {
        templateUrl: 'app/views/inscription.html',
        controller: 'InscriptionController',
        controllerAs: 'inscription'
      })
      .when('/resultats/:idJourney', {
        templateUrl: 'app/views/resultats.html',
        controller: 'ResultatsController',
        controllerAs: 'resultats'
      })
      .when('/messagerie', {
        templateUrl: 'app/views/messagerie.html',
        controller: 'MessagerieController',
        controllerAs: 'messagerie'
      })
      .when('/messagerie/discussion/:idDiscussion', {
        templateUrl: 'app/views/discussion.html',
        controller: 'DiscussionController',
        controllerAs: 'discussion'
      })
      .when('/contact/:idAnnonce', {
        templateUrl: 'app/views/contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'contact'
      })
      .when('/mes-trajets', {
        templateUrl: 'app/views/mesTrajets.html',
        controller: 'MestrajetsController',
        controllerAs: 'mesTrajets'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
